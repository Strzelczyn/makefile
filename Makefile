CC = g++

all: main

main: main.cpp
	$(CC) $< -o $@

clean:
	rm main